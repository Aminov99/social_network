from django.apps import AppConfig


class MyCommunitiesConfig(AppConfig):
    name = 'my_communities'

from django.db import models


# Create your models here.
from my_profile.models import Page

from my_profile.models import ProfilePage

from my_profile.models import Post


class Community(Page):
    title = models.CharField(max_length=20)
    information = models.TextField()
    admins = models.ManyToManyField(ProfilePage, related_name='Администраторы')
    posts = models.ForeignKey(Post, default=None, blank=True, on_delete=models.CASCADE)

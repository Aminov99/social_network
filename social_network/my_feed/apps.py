from django.apps import AppConfig


class MyFeedConfig(AppConfig):
    name = 'my_feed'

from django.urls import path

from my_groups.views import show_all_users

urlpatterns = [
    path('', view=show_all_users),
]
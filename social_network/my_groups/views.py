import datetime

from django.db.models import Q
from django.shortcuts import render

# Create your views here.
from my_profile.models import ProfilePage


def show_all_users(request):
    '''
    Можно добавить следующий фильтр .filter(pk_gte=F(pk)) он будет проверять, больше или равен ли id модели чем id этой
    же модели.
    бесполезная операция, потому что она всегда будет возвращать True.
    '''
    profiles = ProfilePage.objects.all().filter(state='O').filter(user__is_superuser=False).\
        filter(pk__gt=4).\
        filter(Q(first_name__range=('А','Я'))).values(
        'url_address', 'first_name', 'last_name', 'id')  # выведет всех, у кого id>4 и у кого имя начинается с Заглавной буквой киррилицы


    return render(request, 'groups/friend_list.html', {"profiles": profiles})

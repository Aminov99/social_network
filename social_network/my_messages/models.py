import datetime

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils.timezone import now
from django.apps import apps


class Message(models.Model):
    text = models.TextField()
    sender = models.ForeignKey("my_profile.ProfilePage", on_delete=models.CASCADE, blank=True,
                               related_name='sender')  # ProfilePage
    receiver = models.ForeignKey("my_profile.ProfilePage", on_delete=models.CASCADE, blank=True,
                                 related_name='receiver')  # ProfilePage
    date = models.DateTimeField(null=datetime.datetime.now, blank=True, default=now)
    is_read = models.BooleanField(default=False)

from multiprocessing.connection import Client

from django.test import TestCase

# Create your tests here.
from django.urls import reverse
from django.contrib.auth.models import User


class LoginChecker(TestCase):
    def setUp(self):
        User.objects.create_user(username='admin',password='password')

    def test(self):
        response = self.client.login(username='admin', password='password')
        self.assertEqual(response, True)


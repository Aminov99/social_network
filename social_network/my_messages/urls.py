from django.conf.urls import url
from django.shortcuts import render
from django.urls import path

from . import views

urlpatterns = [
    path('list', view=views.show_messages),
    path('list/<int:pk>/', view=views.MessagesView.as_view()),
    path('list/<int:pk>/read/', view=views.read_text),
    path('preparemessage/<int:pk>', view=views.prepare_message, name='prepare_message'),
    path('sendmessage', view=views.send_message, name='send_message')
]

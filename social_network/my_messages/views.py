from django.apps import apps
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect, get_object_or_404

# Create your views here.
from django.utils.timezone import now
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from my_profile.models import ProfilePage
from .models import Message


@login_required
def show_messages(request):
    receiver = ProfilePage.objects.get(user=request.user)
    messages = Message.objects.filter(receiver=receiver).order_by('-date')
    list_of_senders = []
    for message in messages:
        list_of_senders.append(message.sender)
    return render(request, 'mymessages/messages.html', {'messages': messages})


class MessagesView(generic.DetailView):
    model = Message
    context_object_name = 'message'
    template_name = 'mymessages/message.html'

    def get_context_data(self, **kwargs):
        context = super(MessagesView, self).get_context_data(**kwargs)
        message = context['message']
        message.is_read = True
        message.save()
        return context


def read_text(request, pk):
    message = get_object_or_404(Message, id=pk)
    message.is_read = True
    message.save()
    return redirect(to=pk)


@login_required
def prepare_message(request, pk):
    receiver = ProfilePage.objects.filter(pk=pk)
    return render(request, 'mymessages/prepare_message.html', {'receiver': receiver})


@login_required
@csrf_exempt
def send_message(request):
    message = request.POST.get('message', )
    receiverID = request.POST.get('receiverID', )
    rec = get_object_or_404(ProfilePage, id=receiverID)
    sender = get_object_or_404(ProfilePage, user=request.user)
    new_message = Message(text=message, receiver=rec, sender=sender, date=now())
    new_message.save()
    messages = Message.objects.all()
    return HttpResponse(messages, content_type='application/json')

from django.contrib import admin

# Register your models here.
from .models import ProfilePage, Post, Document

admin.site.register(ProfilePage)
admin.site.register(Post)
admin.site.register(Document)
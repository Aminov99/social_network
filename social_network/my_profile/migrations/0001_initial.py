# Generated by Django 2.1.7 on 2019-04-25 11:13

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_of_creating', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Community',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField()),
                ('url_address', models.URLField()),
                ('status', models.CharField(max_length=100)),
                ('website', models.URLField()),
                ('page_picture', models.ImageField(upload_to='')),
                ('title', models.CharField(max_length=40)),
                ('info', models.TextField()),
                ('banner', models.ImageField(upload_to='')),
                ('contacts', models.CharField(max_length=80)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Discussion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Comment')),
            ],
        ),
        migrations.CreateModel(
            name='Document',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('size', models.IntegerField()),
                ('type', models.CharField(max_length=4)),
                ('name', models.CharField(max_length=40)),
                ('storage_name', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('country', models.CharField(max_length=40)),
                ('city', models.CharField(max_length=40)),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=40)),
                ('text', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='ProfilePage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField()),
                ('url_address', models.URLField()),
                ('status', models.CharField(max_length=100)),
                ('website', models.URLField()),
                ('page_picture', models.ImageField(upload_to='')),
                ('first_name', models.CharField(max_length=40)),
                ('last_name', models.CharField(max_length=40)),
                ('middle_name', models.CharField(max_length=40)),
                ('date_of_birth', models.DateField()),
                ('extra_info', models.TextField()),
                ('albums', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Album')),
                ('subscribers', models.ManyToManyField(to='my_profile.ProfilePage')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Wallpost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('posts', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Post')),
            ],
        ),
        migrations.CreateModel(
            name='Group',
            fields=[
                ('community_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='my_profile.Community')),
                ('open_status', models.CharField(choices=[('o', 'open'), ('c', 'close')], max_length=2)),
                ('menu', models.TextField()),
            ],
            options={
                'abstract': False,
            },
            bases=('my_profile.community',),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('document_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='my_profile.Document')),
                ('resolution', models.CharField(max_length=10)),
            ],
            bases=('my_profile.document',),
        ),
        migrations.CreateModel(
            name='Meeting',
            fields=[
                ('community_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='my_profile.Community')),
                ('date', models.DateTimeField()),
                ('improbable', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
                ('probable', models.ManyToManyField(related_name='probable', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
            bases=('my_profile.community',),
        ),
        migrations.CreateModel(
            name='Music',
            fields=[
                ('document_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='my_profile.Document')),
                ('album', models.CharField(max_length=40)),
            ],
            bases=('my_profile.document',),
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('document_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='my_profile.Document')),
                ('duration', models.CharField(max_length=10)),
            ],
            bases=('my_profile.document',),
        ),
        migrations.AddField(
            model_name='profilepage',
            name='wallpost',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Wallpost'),
        ),
        migrations.AddField(
            model_name='post',
            name='document',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Document'),
        ),
        migrations.AddField(
            model_name='post',
            name='repost',
            field=models.ManyToManyField(to='my_profile.ProfilePage'),
        ),
        migrations.AddField(
            model_name='community',
            name='address',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Location'),
        ),
        migrations.AddField(
            model_name='community',
            name='admins',
            field=models.ManyToManyField(related_name='admins', to='my_profile.ProfilePage'),
        ),
        migrations.AddField(
            model_name='community',
            name='albums',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Album'),
        ),
        migrations.AddField(
            model_name='community',
            name='discussions',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Discussion'),
        ),
        migrations.AddField(
            model_name='community',
            name='subscribers',
            field=models.ManyToManyField(to='my_profile.ProfilePage'),
        ),
        migrations.AddField(
            model_name='community',
            name='wallpost',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Wallpost'),
        ),
        migrations.AddField(
            model_name='comment',
            name='like',
            field=models.ManyToManyField(related_name='like', to='my_profile.ProfilePage'),
        ),
        migrations.AddField(
            model_name='comment',
            name='user_profile',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='my_profile.ProfilePage'),
        ),
        migrations.AddField(
            model_name='profilepage',
            name='music',
            field=models.ManyToManyField(to='my_profile.Music'),
        ),
        migrations.AddField(
            model_name='profilepage',
            name='videos',
            field=models.ManyToManyField(to='my_profile.Video'),
        ),
        migrations.AddField(
            model_name='community',
            name='music',
            field=models.ManyToManyField(to='my_profile.Music'),
        ),
        migrations.AddField(
            model_name='community',
            name='videos',
            field=models.ManyToManyField(to='my_profile.Video'),
        ),
        migrations.AddField(
            model_name='album',
            name='photos',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='my_profile.Image'),
        ),
    ]

# Generated by Django 2.1.7 on 2019-05-04 06:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('my_feed', '0001_initial'),
        ('my_profile', '0013_document_post'),
    ]

    operations = [
        migrations.AddField(
            model_name='profilepage',
            name='feed_list',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='my_feed.FeedList'),
        ),
    ]

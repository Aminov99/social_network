import datetime

from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.utils.timezone import now

status = (('o', 'open'), ('c', 'close'))


class Page(models.Model):
    creation_date = models.DateTimeField(blank=True, null=True)
    url_address = models.CharField(max_length=20)
    status = models.CharField(max_length=100)
    # website = models.URLField()
    # subscribers = models.ManyToManyField('ProfilePage', blank=True, default=None)
    # albums = models.ForeignKey('Album', on_delete=models.CASCADE, default=None, blank=True)
    # music = models.ManyToManyField('Music', blank=True, default=None)
    # videos = models.ManyToManyField('Video', blank=True, default=None)
    # wallpost = models.ForeignKey('Wallpost', on_delete=models.CASCADE, blank=True, default=None)
    page_picture = models.ImageField(upload_to='images/', null=True, blank=True)

    class Meta:
        abstract = True


class ProfilePage(Page):
    STATE_OF_PRIVACY = (('O', "Открытый"),
                        ('C', "Закрытый"))
    state = models.CharField(max_length=1, choices=STATE_OF_PRIVACY, default="O")
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    date_of_birth = models.DateField()
    extra_info = models.TextField()

    # class Community(Page):
    #     title = models.CharField(max_length=40)
    #     info = models.TextField()
    #     discussions = models.ForeignKey('Discussion', on_delete=models.CASCADE)
    #     address = models.ForeignKey("Location", on_delete=models.CASCADE)
    #     banner = models.ImageField()
    #     admins = models.ManyToManyField('ProfilePage', related_name="admins")
    #     contacts = models.CharField(max_length=80)
    #
    #
    # class Discussion(models.Model):
    #     comment = models.ForeignKey("Comment", on_delete=models.CASCADE)
    #
    #
    # class Group(Community):
    #     open_status = models.CharField(max_length=2, choices=status)
    #     menu = models.TextField()
    #
    #
    # class Meeting(Community):
    #     date = models.DateTimeField()
    #     probable = models.ManyToManyField(User, related_name="probable")
    #     improbable = models.ManyToManyField(User)
    #
    #


class Document(models.Model):
    size = models.IntegerField(default=0)
    type = models.CharField(max_length=4)
    name = models.CharField(max_length=40)
    storage_name = models.CharField(max_length=100)
    value = models.FileField(upload_to='files', blank=True)
    #
    #
    # class Music(Document):
    #     album = models.CharField(max_length=40)
    #
    #
    # class Image(Document):
    #    resolution = models.CharField(max_length=10)
    #
    #
    # class Comment(models.Model):
    #     user_profile = models.OneToOneField('ProfilePage', on_delete=models.CASCADE)
    #     text = models.TextField()
    #     like = models.ManyToManyField('ProfilePage', related_name="like")
    #
    #
    # class Album(models.Model):
    #     photos = models.ForeignKey(Image, on_delete=models.CASCADE)
    #     date_of_creating = models.DateTimeField()
    #
    #
    # class Wallpost(models.Model):
    #     posts = models.ForeignKey('Post', default=None, blank=True, on_delete=models.CASCADE)
    #
    #


class Post(models.Model):
    title = models.CharField(max_length=40)
    text = models.TextField()
    document = models.ForeignKey(Document, on_delete=models.CASCADE, blank=True, default=None, null=True)
    author = models.ForeignKey(ProfilePage, on_delete=models.CASCADE, blank=True, null=True)
    creation_date = models.DateTimeField(null=True, blank=True)
    likes = models.ManyToManyField('my_profile.Like', blank=True)

    # class Video(Document):
    #     duration = models.CharField(max_length=10)  # 4:23
    #
    #
    # class Location(models.Model):
    #     country = models.CharField(max_length=40)
    #     city = models.CharField(max_length=40)

    class Like(models.Model):
        profile = models.ForeignKey(ProfilePage,
                                    related_name='likes',
                                    on_delete=models.CASCADE)

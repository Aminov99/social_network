from django.conf.urls import url
from django.urls import path, re_path

from my_profile.views import show_profile, profile_page

from my_profile.views import post_create

from my_profile.views import unlock, lock

from my_profile.views import delete_post

urlpatterns = [
    path('unlock', view=unlock, name='unlock'),
    path('lock', view=lock, name='lock'),
    path('', view=show_profile, name='show_profile'),
    path('create', view=post_create, name='post_create'),
    re_path(r'^(?P<url>[a-z]{1,10})/$', view=profile_page, name='profile_page'),
    path('deletepost', view=delete_post, name='delete_post')
]

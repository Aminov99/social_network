from django.contrib.auth.decorators import login_required
from django.core.serializers import json
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from my_profile.models import ProfilePage
from my_profile.models import Post
from django.utils import timezone

from social_network.forms import RegistrationProfileForm


@login_required(login_url='/login')
def show_profile(request):
    if request.method == 'GET':
        if ProfilePage.objects.filter(user=request.user).exists():
            profile = ProfilePage.objects.get(user=request.user)
            posts = Post.objects.filter(author=profile).order_by('-creation_date')
            return render(request, 'profile/profile.html', {'profile': profile, "posts": posts})
        else:
            form = RegistrationProfileForm()
            return render(request, 'profile/yourself_form.html', {'form': form})
    if request.method == 'POST':
        form = RegistrationProfileForm(request.POST)
        if form.is_valid():
            print(form.cleaned_data)
            profile_form = form.save(commit=False)
            profile_form.user = request.user
            profile_form.save()
            return redirect('/profile')
        else:
            return HttpResponse(form.errors)


@csrf_exempt
@login_required(login_url='/login')
def post_create(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        text = request.POST.get('text')
        profile = ProfilePage.objects.get(user=request.user)
        post = Post(title=title, text=text, author=profile, creation_date=timezone.now())
        post.save()
        return redirect('show_profile')
    return render(request, 'profile/post_create.html')


@login_required(login_url='/login')
def profile_page(request, url):
    print(url)
    profile = ProfilePage.objects.get(url_address=url)
    posts = Post.objects.filter(author=profile).order_by('-creation_date')
    return render(request, 'profile/profile.html', {'profile': profile, 'posts': posts})




def unlock(request):
    profile_page = ProfilePage.objects.get(user=request.user)
    profile_page.state = 'O'
    profile_page.save()
    return HttpResponse("{success: very good}", content_type='application/json')


@login_required(login_url='/login')
def lock(request):
    profile_page = ProfilePage.objects.get(user=request.user)
    profile_page.state = 'C'
    profile_page.save()
    return HttpResponse("{success: fdsa}", content_type='application/json')

@csrf_exempt
def delete_post(request):
    id = request.POST.get('id')
    post = Post.objects.get(pk=id)
    post.delete()
    return HttpResponse("{success:fdsa}", content_type='application/json')

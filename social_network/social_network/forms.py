from django.contrib.auth import password_validation
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import forms

from my_profile.models import ProfilePage
from django import forms


class RegistrationProfileForm(forms.ModelForm):
    class Meta:
        model = ProfilePage
        fields = ['first_name', 'last_name',
                  'date_of_birth', 'extra_info', 'url_address', 'page_picture', 'status']

    def clean(self):
        if len(self.cleaned_data.get('url_address')) >= 10:
            raise forms.ValidationError(
                'url должен быть меньше 10 знаков и содержать только строчные латинские символы')


class UserCreationForm(forms.ModelForm):
    login = forms.CharField(max_length=10)
    password1 = forms.CharField(
        label='Пароль',
        widget=forms.PasswordInput
    )
    password2 = forms.CharField(
        label='Подтверждение',
        widget=forms.PasswordInput
    )

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Пароль и подтверждение не совпадают')
        return password2

    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user

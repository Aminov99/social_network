"""social_network URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth import views as auth_views

from social_network import views
from social_network.views import sign_up

urlpatterns = [
    path('admin/', admin.site.urls),
    path('messages/', include('my_messages.urls')),
    url(r'^login', view=auth_views.LoginView.as_view(template_name='login/auth.html'),
        name='login'),
    path('profile/', include('my_profile.urls')),
    path('groups/', include('my_groups.urls')),
    path('feed/', include('my_feed.urls')),
    path('music/', include('my_communities.urls')),
    path('registration/', view=sign_up,name='sign_up'),
]
